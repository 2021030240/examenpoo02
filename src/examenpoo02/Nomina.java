/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpoo02;

/**
 *
 * @author _Windows_
 */
public class Nomina {
    private int numDocente;
    private String nombre;
    private String domicilio;
    private int nivel;
    private float pagoBase;
    private float horasImp;

    public Nomina() {
        this.numDocente = 0;
        this.nombre = "";
        this.domicilio = "";
        this.nivel = 0;
        this.pagoBase = 0.0f;
        this.horasImp = 0.0f;
    }

    public Nomina(int numDocente, String nombre, String domicilio, int nivel, float pagoBase, float horasImp) {
        this.numDocente = numDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagoBase = pagoBase;
        this.horasImp = horasImp;
    }
    public Nomina(Nomina otro) {
        this.numDocente = otro.numDocente;
        this.nombre = otro.nombre;
        this.domicilio = otro.domicilio;
        this.nivel = otro.nivel;
        this.pagoBase = otro.pagoBase;
        this.horasImp = otro.horasImp;
    }

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoBase() {
        return pagoBase;
    }

    public void setPagoBase(float pagoBase) {
        this.pagoBase = pagoBase;
    }

    public float getHorasImp() {
        return horasImp;
    }

    public void setHorasImp(float horasImp) {
        this.horasImp = horasImp;
    }
    
    
    public float calcularPago(){
        
        float pago=0;
        if (this.nivel == 1) {
            pago = this.pagoBase * 0.30f;
            pago = pago + this.pagoBase;
            pago = pago * this.horasImp;
            return pago;
        }
        if (this.nivel == 2) {
            pago = this.pagoBase * 0.50f;
            pago = pago + this.pagoBase;
            pago = pago * this.horasImp;
            return pago;
        }
        if (this.nivel == 3) {
            pago = this.pagoBase*2f;
            pago = pago * this.horasImp;
            return pago;
        }
        return 0;
    }
    public float calcularImpuesto(){
        return this.calcularPago()*0.16f;   
    }
    public float calcularBono(int hijos){
        float pagoF = 0.0f;
        if (hijos >= 1 && hijos <= 2) {
            return 0.05f;
        }
        if (hijos >=3 && hijos <= 5) {
            return 0.10f;
        }
        if (hijos > 5) {
            return 0.20f;
        }
        return 0;
    }
}
